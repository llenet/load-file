import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../models/User";

@Injectable({
  providedIn: "root",
})
export class UsersService {
  constructor(private http: HttpClient) {
  }

  private usersUrl = "http://localhost:8080/users"

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl)
  }

  public save(user: User): Observable<User> {
    return this.http.post<User>(this.usersUrl, user)
  }

  public addFile(userId: number, file: File) {
    const data = new FormData();
    data.append('file', file)
    return this.http.post<File>(`${this.usersUrl}/${userId}/file`, data);
  }
}
