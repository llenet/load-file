import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UsersService} from "../services/users.service";
import {User} from "../models/User";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent {
  constructor(private userService: UsersService) {
  }

  userForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    age: new FormControl('', Validators.required)
  });

  onSubmit() {
    this.userService.save({
      firstName: this.userForm.controls["firstName"].value!!,
      lastName: this.userForm.controls["lastName"].value!!,
      age: parseInt(this.userForm.controls["age"].value!!)
    }).subscribe()
  }
}
