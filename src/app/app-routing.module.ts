import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UsersComponent} from "./users/users.component";
import {AddUserComponent} from "./add-user/add-user.component";
import {AddFileComponent} from "./add-file/add-file.component";

const routes: Routes = [
  {path : "", component: UsersComponent},
  {path : "addFile", component: AddFileComponent},
  {path : "addUser", component: AddUserComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
