import { Component } from '@angular/core';
import {UsersService} from "../services/users.service";
import {Observable} from "rxjs";
import {User} from "../models/User";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  users$ : Observable<User[]>
  constructor(userService: UsersService) {
    this.users$ = userService.findAll();
  }

}
