import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UsersService} from "../services/users.service";

@Component({
  selector: 'app-add-file',
  templateUrl: './add-file.component.html',
  styleUrls: ['./add-file.component.scss']
})
export class AddFileComponent {

  users$
  selectedFile?: File
  constructor(private userService: UsersService) {
    this.users$ = userService.findAll();
  }

  fileForm = new FormGroup({
    userId: new FormControl('', Validators.required),
    file: new FormControl('', Validators.required),
  });

  onSubmit() {
    this.userService.addFile(parseInt(this.fileForm.controls.userId.value!!), this.selectedFile!!).subscribe()
  }

  public onFileChanged(event: any) {
    this.selectedFile = event.target.files.item(0);
  }
}
